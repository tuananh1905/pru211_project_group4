﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using static System.Net.Mime.MediaTypeNames;

public class TextSwitcher : MonoBehaviour
{
    public string[] listTextToSet = { "Chào mừng đến với thế giới của chúng tôi, nơi đầy nguy hiểm và thử thách.Bạn là một chiến binh tinh nhuệ, và nhiệm vụ của bạn là tiêu diệt những con quái vật đang xâm chiếm thế giới này.Sức mạnh và trí tuệ của bạn sẽ được đặt vào thử thách trong mỗi màn chơi.Sau mỗi màn, bạn sẽ thu thập được vũ khí mới để giúp bạn tiếp tục chiến đấu.",
    "Tuy nhiên, nhiệm vụ cuối cùng của bạn là tìm kiếm và giành được kho báu của trò chơi. Nó được giấu kín ở màn chơi cuối cùng và bị canh giữ bởi một con boss lớn và đáng sợ. Hãy chuẩn bị tinh thần và sức mạnh của bạn, bởi vì nhiệm vụ này sẽ không dễ dàng.",
        "Hãy sẵn sàng đối mặt với một thế giới đầy rẫy những sinh vật ác liệt, những con quái vật khổng lồ và những thử thách đầy nguy hiểm. Bạn sẽ phải chiến đấu để sống sót, chiến đấu để chiến thắng, và cuối cùng là chiến đấu để giành chiến thắng tuyệt đối. Hãy sẵn sàng để bắt đầu cuộc phiêu lưu của bạn trong thế giới của chúng tôi."};
    private TextMeshProUGUI textMeshProComponent;
    public float fontSizeToSet = 20;
    private int currentTextIndex = 0;
    
    // Start is called before the first frame update
    void Start()
    {
        textMeshProComponent = GetComponent<TextMeshProUGUI>();
        textMeshProComponent.text = listTextToSet[currentTextIndex];
        textMeshProComponent.fontSize = fontSizeToSet;
        textMeshProComponent.color = Color.red;
       
       
    }
    public void NextText()
    {
        currentTextIndex++;
        if (currentTextIndex >= listTextToSet.Length)
        {
            currentTextIndex = 0;
        } 
        textMeshProComponent.SetText(listTextToSet[currentTextIndex]);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
