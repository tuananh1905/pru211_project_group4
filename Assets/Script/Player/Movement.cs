using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public float moveSpeed = 2f;
    public float runSpeed = 5f;
    Vector2 movement;
    public Rigidbody2D rb;
    public Animator anim;
    private bool isfacingRight = true;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }
    private void FixedUpdate()
    {
        flip();
        if (Input.GetKey(KeyCode.J))
        {
            anim.SetTrigger("AttackTrigger");
            rb.MovePosition(rb.position);
        }
        else move();
    }
    void flip()
    {
        if (isfacingRight && movement.x < 0 || !isfacingRight && movement.x > 0)
        {
            isfacingRight = !isfacingRight;
            Vector3 scale = transform.localScale;
            scale.x = scale.x * -1;
            transform.localScale = scale;
        }
    }
    void move()
    {
        movement.x = Input.GetAxis("Horizontal");
        movement.y = Input.GetAxis("Vertical");
        anim.SetFloat("Horizontal", movement.x);
        anim.SetFloat("Vertical", movement.y);
        rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);
        anim.SetFloat("Speed", movement.sqrMagnitude);
        if (Input.GetKey(KeyCode.LeftShift))
        {
            anim.SetTrigger("RunTrigger");
            rb.MovePosition(rb.position + movement * runSpeed * Time.fixedDeltaTime);
        }
    }
}
